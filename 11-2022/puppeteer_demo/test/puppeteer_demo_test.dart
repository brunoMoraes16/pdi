import 'package:puppeteer/puppeteer.dart';
import 'package:puppeteer_demo/resources.dart';
import 'package:test/test.dart';

void main() {
  late Browser browser;
  late Page page;

  setUp(() async {
    browser = await puppeteer.launch();
    page = await browser.page();
  });

  tearDown(() async {
    await browser.close();
  });

  test('Google HomePage', () async {
    await page.goto(url, wait: Until.networkIdle);
    var title = await page.title;
    expect(title, 'Google');
  });
}
