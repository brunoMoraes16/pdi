import 'package:puppeteer/puppeteer.dart';

extension PageExtension on Page {
  Future<ElementHandle> $xp(String selector) async {
    return (await $x(selector)).first;
  }
}
