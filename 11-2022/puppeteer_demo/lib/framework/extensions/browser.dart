import 'package:puppeteer/puppeteer.dart';

extension BrowserExtension on Browser {
  Future<Page> page({int index = 0, Device? device}) async {
    final page = (await pages)[index];
    if (device != null) await page.emulate(device);
    return page;
  }
}
