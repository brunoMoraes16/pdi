import 'dart:io';
import 'dart:typed_data';

import 'package:uuid/uuid.dart';

extension ScreenshotExtension on Future<Uint8List> {
  Future<File> toFile(
    String path, {
    bool flush = false,
    FileMode mode = FileMode.write,
  }) async {
    final bytes = await this;
    await File(path).create(recursive: true);
    return await File(path).writeAsBytes(
      bytes,
      flush: flush,
      mode: mode,
    );
  }

  Future<File> save(String dir) async {
    return await toFile('$dir/${Uuid().v4().toString()}.png', flush: true);
  }
}
