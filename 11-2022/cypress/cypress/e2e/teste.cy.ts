describe('Google.com', () => {

  it('search cypress in Google.com', () => {
    cy.visit('https://www.google.com/');

    var element = cy.get('input[name=q]');
    cy.title().should('eq', 'Google');
    element.type('Cypress.io{enter}');
    
    cy.get('h3').should('contain', 'JavaScript End to End Testing Framework').should('be.visible');
    cy.title().should('contain', 'Cypress.io');
  });

})